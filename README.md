You need python 3 and virtualenv in order to work with this code

Create a virtualenv by doing:
python3 -m venv CLOUDY_ML

Then activate the virtual environment by doing:
source CLOUDY_ML/bin/activate

Once you activate the environment, you should install the necessary libraries by doing:
pip install -r requirements.txt 






from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from matplotlib.colors import LogNorm
from sklearn.model_selection import KFold
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.callbacks import ModelCheckpoint

#Constants
Lsun = 3.8270e33 #Solar luminosity in ergs/s
pc2cm = 3.086e18 #parsecs to cm

#Load in the O3_5007 nebular line data
O3_5007 = np.loadtxt('O3_5007_update_fdx.dat')

#Note that because CLOUDY prints log data, values that equal 0.0
#Actually have zero flux so we set these to -50.0
O3_5007[O3_5007==0.0] = -50.0

#Load in the cell widths in cm
cell_dx   = np.loadtxt('cell_dx.dat')

#Load in the cell properties
#Organized as density, temperature, metallicity, SED params 1-6
#Everything is in log except for the radiation scale factors so we take the log of these as well
features  = np.loadtxt('all_features.dat')
#Log scale the radiation scale factors
features[:,3:]=np.log10(features[:,3:])

#CLOUDY saves the data as flux in erg/s/cm^2 so let's turn this into a luminosity
L_O3_5007 = (10.0**O3_5007)*(10.0**cell_dx)*(10.0**cell_dx)

#Plot a 2D histogram of density and temperature weighted by L_O3
plt.hist2d(features[:,0],features[:,1],bins=50,norm=LogNorm(),weights=L_O3_5007,vmin=1e33,vmax=1e41)
plt.colorbar()
plt.xlabel(r'$\log_{10}(n_{\rm H}/cm^{-3})$')
plt.ylabel(r'$\log_{10}(T/K)$')

#What fraction of the data will we use for our test set
#In this example we use an 80% - 20% split
test_frac = 0.2

#Many of our algorithms use a random number generator so for reproducibility
#we should set a random state.  If we change this number, we will get slightly
#different values
rs        = 42

#Use the sklearn function to split the data into our test and train sample
#We set shuffle = True so that it doesn't just take the first 80%
X_train, X_test, y_train, y_test = train_test_split(features, L_O3_5007, test_size=test_frac, random_state=rs, shuffle=True)

#You can check to make sure this worked by checking the lengths of the arrays
print('Train frac:',len(X_train)/len(features))
print('Test frac:',len(X_test)/len(features))


#Now lets start with a simple random forest

#This algorithm is parallelizable so we will use 4 CPUs for training
n_cpus = 4

#How many trees do we want in the forest
n_trees = 10

#Initialize the random forest
rf_regr = RandomForestRegressor(n_jobs=n_cpus,random_state=rs,max_features='auto',n_estimators=n_trees)

#Train the forest on the training set
rf_regr.fit(X_train, y_train)

#Make the prediction based on the unseen data
y_pred = rf_regr.predict(X_test)

#Calculate the fractional difference in total luminosity of the cloud as
# (prediction - truth)/truth
frac_diff = np.abs((y_pred.sum() - y_test.sum())/y_test.sum())
print('Percentage Difference:',round(frac_diff*100.0,4),'%')


#Make a 2D histogram comparing the errors
figure()
plt.hist2d(np.log10(y_test),np.log10(y_pred),bins=50,norm=LogNorm(),range=((30,41),(30,41)))
plt.colorbar()
plt.xlabel(r'$\log_{10}(L_{\rm [OIII]}/erg/s)\ (True)$')
plt.ylabel(r'$\log_{10}(L_{\rm [OIII]}/erg/s)\ (Predict)$')
plt.title('Linear Scaled')


#In the previous example, we trained on the luminosity, what if we train on the log of the luminosity

#Train the forest on the training set using log this time
rf_regr.fit(X_train, np.log10(y_train))

#Make the prediction based on the unseen data
y_pred = rf_regr.predict(X_test)

#Calculate the fractional difference in total luminosity of the cloud as
# (prediction - truth)/truth
frac_diff = np.abs(((10.0**y_pred).sum() - y_test.sum())/(y_test.sum()))
print('Percentage Difference:',round(frac_diff*100.0,4),'%')

#So we can see that in this case, the fractional difference went up quite considerably.
#There is a tradeoff here, when we fit for log, we are treating all cells equally but the OIII
#luminosity is dominated by the brightest cells in the cloud, hence even though the mean squared
#error goes up when we fit for luminosity rather than log10(luminosity), it probably makes sense to
#use the linear scaled value.

#Make a 2D histogram comparing the errors.  Here we can see the plot looks better
figure()
plt.hist2d(np.log10(y_test),y_pred,bins=50,norm=LogNorm(),range=((30,41),(30,41)))
plt.colorbar()
plt.xlabel(r'$\log_{10}(L_{\rm [OIII]}/erg/s)\ (True)$')
plt.ylabel(r'$\log_{10}(L_{\rm [OIII]}/erg/s)\ (Predict)$')
plt.title('Log Scaled')


#Let's try and example using K-folding

#How many folds should we use?  It sort of depends on the amount of data that you have
#and how biased the data is.  10 is sort of the industry standard but we will use 5 here
#because it takes less time.
n_folds = 5

#Initialize the K-folding
kf = KFold(n_splits=n_folds, random_state=rs)

#We will run 5-fold cross validaton to determine what is the optimal number of trees in the forest
#In this case, we loop over forests consisting of 3, 5, and 10 trees

#List containing the number of trees
n_trees = [3, 5, 10]

#Loop ovdr the number of trees in the forest
for nt in n_trees:
    
    #Keep track of the number of folds
    counter = 0
    
    print('We are currently working with',nt,'trees')
    
    #Create and empty array containing the errors
    my_errors = []
    
    #Initialize the random forest to have the correct number of trees
    rf_regr = RandomForestRegressor(n_jobs=n_cpus,random_state=rs,max_features='auto',n_estimators=nt)
    
    #kf.split actually splits our data into the 5 folds that we want
    #We now loop over each sub-training and sub-test set in the splits
    
    for train_index, test_index in kf.split(X_train):
        
        #Keep track of the number of folds
        counter += 1
        
        #Train the random forest on the subset of the training data
        rf_regr.fit(X_train[train_index], y_train[train_index])
        
        #Make predictions for the subset of the training data in the cross validation set
        y_pred = rf_regr.predict(X_train[test_index])
        
        #Calculate the fractional difference in total luminosity of the cloud as
        # (prediction - truth)/truth
        frac_diff = np.abs((y_pred.sum() - y_train[test_index].sum())/y_train[test_index].sum())
        print('Percentage Difference for fold',counter,':',round(frac_diff*100.0,4),'%')
        
        #Append the error to the array
        my_errors.append(frac_diff)

    #Print the mean and standard deviation of all of the 5 folds
    print('Mean % difference:',np.array(my_errors).mean()*100)
    print('Sigma % difference:',np.array(my_errors).std()*100)



# create neural network model
model = Sequential() #This is the standard feedforward neural netowrk

#This is the first hidden layer, notice how we have to give it an input_dimension
#In our case the input dimension is the number of columns in our feature array
#I have selected this layer to have 15 neurons.  kernel_initializer initializes
#The weights as a gaussian with mean 0 and std of 0.01 (I think).  We also need
#An activation function which in our case is relu
model.add(Dense(15, input_dim=len(X_train[0]), kernel_initializer='normal', activation='relu'))

#This is the second hidden layer. It has the same properties as the first but we don't need to
#Tell it how many inputs there will be.  The code takes care of it for us
model.add(Dense(15, kernel_initializer='normal', activation='relu'))

#This is the output layer so it only has 1 neuron since we are only trying to predice 1
#luminosity.  Because this is a regression problem, we do not use an activation function
#on this layer
model.add(Dense(1, kernel_initializer='normal'))
    
#Finally We have to compile the model.  In our case, we use the mean squared error
#as our loss function, we select adam as our optimizer
model.compile(loss='mean_squared_error', optimizer='adam')


#Set checkpointer so we can save the model. The important parameters are the filepath and period
#The period sets the number of epochs between each save
checkpointer = ModelCheckpoint(filepath='O3_neural_netork.h5',verbose=1,save_best_only=False,mode='auto',period=10)


#Train the model
#Batch size controls the number of subsamples of the training set we use to update the weights.
#You shoudl starts with small values and may consider increasing this as training proceeds.
#The number of epochs sets how long to train for.  An epoch is when we have cycled through all batches.
#Therefore, the number of updates/epoch = number cloudy calculations in training set / batch size
#We set shuffle to true to it shuffles the batches.  Finally callbacks tells us to save the model
model.fit(X_train, np.log10(y_train), epochs=150, batch_size=5000,verbose=1,shuffle=True,callbacks=[checkpointer])

#Load back in our model
model = load_model('O3_neural_netork.h5')

#Now make the prediction
#We need to reshape the array because keras puts it in a format that's less easy for us
y_pred = model.predict(X_test).reshape(len(X_test),)

#Calculate the fractional difference in total luminosity of the cloud as
# (prediction - truth)/truth
frac_diff = np.abs(((10.0**y_pred).sum() - y_test.sum())/(y_test.sum()))
print('Percentage Difference:',round(frac_diff*100.0,4),'%')





